$repositories = (aws ecr describe-repositories) | ConvertFrom-Json
$imageName = "%config.ImageName%"

if ($imageName -notin $repositories.repositories.repositoryName) {
    Write-Output "Creating ECR Repository ${imageName}..."
    aws ecr create-repository --repository-name $imageName
}
else{
    Write-Output "ECR Repository ${imageName} already exists, skipping."
}
#this is used for poweshell build step in teamcityy